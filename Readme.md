#Take Home Test from Viz.ai, Inc.

Your task is to write a python script that receives the URL as a command-line argument and:

1. downloads the file

_completed - files are in the Bitbucket repository

1. arranges the files according to the DICOM hierarchy in an appropriate directory structure (patient/study/series). Note that patient names were replaced with IDs to protect their privacy.

            _completed - again the files are in the repository, divided into 6 folders, 5 of which have 1 subfolder, the other 
            has 2. Inside each sub-bolder are the relevant dcm files.

1. performs the following tasks or answers the following questions:

* generate a list of patients, their age and sex

    _in the repository is a file called Patients Data.csv which shown the details of the 6 patients

* how long does a CT scan take on average?

    _Using 'ExposureTime' as the measure of the length of time the scan takes, it appears that there are 2 values 
    1000 or 2000, the average of all these values is 1827.5862068965516

* how many different hospitals do the data come from?

    _Using 'InstitutionName' - it appears there are 2 hospitals: ['INST_B' 'INST_A' 'IHBAS']

* you might find the pydicom module very handy, as well as the following tags:

        * PatientName
        * StudyInstanceUID (unique identifier per study)
        * SeriesInstanceUID (unique identifier per series)
        * PatientAge
        * PatientSex
        * InstitutionName

1. Next, use a DICOM viewer suitable for your OS to view the scans. anything seems particularly interesting? You're obviously not expected to make a medical diagnosis, just spend 5-10 minutes browsing through the scans and share your general impressions.

    _using https://www.imaios.com/en/Imaios-Dicom-Viewer - There seems to be a large quantity of white material - not 
    sure what this represents. The scans seem very clear, easy to read.

1. one of the patients has more than one series. Use a DICOM viewer suitable to your OS to explore the series and try explain the differences.

    _as one series had a significantly higher number of scans, it seemed obvious that this series was looking in 
    greater detail at the brain concerned. Looking at the underlying data in an effort to understand more of what 
    I was looking it, there are 3 columns of data that are specifically related to one series or the other:

        * Exposure  
        * SeriesDescription 
        * SliceThickness

    This leads me to the conclusion that this patient has been scanned a second time, with much thinner slices
    being recorded on the second series.

1. Lastly, please open a bitbucket account if you don't have one and share your code with us via bitbucket.

    _completed - 