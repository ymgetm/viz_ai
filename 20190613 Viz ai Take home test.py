### 13.6.19 - 18:20 to 19:15
### 15.6.19 - 12:00 to 15:00


import pydicom
import tarfile
import urllib.request
import os
from os.path import abspath, dirname, join
import fnmatch
from os import walk
import pandas as pd


################# Creating necessary variables
outpath="C:/Users/mmoalem/Documents/Personal/Python/Viz_ai/"

################# downloads the file

thetarfile = "https://s3.amazonaws.com/viz_data/DM_TH.tgz"
ftpstream = urllib.request.urlopen(thetarfile)
tar = tarfile.open(fileobj=ftpstream, mode="r|gz")
tar.extractall(path=outpath)
tar.close()

################# Functions needed to read dicom files

def get_files(base, pattern):
    """Return all files from a set of sources.
    Parameters
    ----------
    base : str
        Base directory to recursively search.
    pattern : str
        A string pattern to filter the files. Default is "*" and it will return
        all files.
    Returns
    -------
    files : list of str
        The list of filenames matched.
    """

    # if the user forgot to add them
    pattern = "*" + pattern + "*"

    files = []
    for root, dirnames, filenames in walk(base):
        for filename in filenames:
            filename_filter = fnmatch.filter([join(root, filename)],
                                             pattern)
            if len(filename_filter):
                files.append(filename_filter[0])

    return files

def get_testdata_files(pattern="*"):
    """Return test data files from pydicom data root.
    Parameters
    ----------
    pattern : str, optional (default="*")
        A string pattern to filter the files
    Returns
    -------
    files : list of str
        The list of filenames matched.
    """

    data_path = join(DATA_ROOT, 'Viz_ai')

    files = get_files(base=data_path, pattern=pattern)
    files = [filename for filename in files if not filename.endswith('.py')]

    return files


################# Creating a path to the save folder

pattern="*"
DATA_ROOT = abspath(dirname('__file__'))
data_path = join(DATA_ROOT, 'Viz_ai')

files = get_files(base=data_path, pattern=pattern)


################# For some of the questions, it is easier for me to have the relevant data points saved in a pandas dataframe.
################# This will make pulling the information much faster


important_info = pd.DataFrame(columns = ['File', 'PatientName', 'PatientAge', 'PatientSex', 'InstitutionName',   
                                             'SeriesInstanceUID', 'StudyInstanceUID', 'ExposureTime'])

i = 0
for f in files:
    filename = get_testdata_files(f)[0]
    dcm_name = filename[-14:]
    ds = pydicom.dcmread(filename)
    important_info_ExposureTime = ds.ExposureTime
    important_info_InstitutionName = ds.InstitutionName 
    important_info_PatientAge = ds.PatientAge
    important_info_PatientName = ds.PatientName
    important_info_PatientSex = ds.PatientSex
    important_info_SeriesInstanceUID = ds.SeriesInstanceUID
    important_info_StudyInstanceUID = ds.StudyInstanceUID
    important_info_File = f
    important_info.loc[i] = [important_info_File, important_info_PatientName, important_info_PatientAge, 
                             important_info_PatientSex, important_info_InstitutionName, important_info_SeriesInstanceUID, 
                             important_info_StudyInstanceUID, important_info_ExposureTime]
	
	
	################# arranges the files according to the DICOM hierarchy in an appropriate directory structure
	################# Unfortanately Image_for_TH.png did was nto visible - hwoever the description given was understandable
	
    save_path = (data_path + '\\' + str(important_info_PatientName) + '\\' + str(important_info_StudyInstanceUID) + 
                                                                        '\\' + str(important_info_SeriesInstanceUID))
    if not os.path.exists(save_path):
        os.makedirs(save_path)
                
    ds.save_as(save_path + '\\' + dcm_name)
    i +=1


################# 1) generate a list of patients, their age and sex

important_info['PatientName'] = important_info['PatientName'].astype(str)

patients_data = important_info[['PatientName', 'PatientAge', 'PatientSex']].drop_duplicates()
patients_data.to_csv(data_path + '\Patients Data.csv', encoding='utf-8', index=False)


################# 2) how long does a CT scan take on average? 


print (important_info['ExposureTime'].mean())

################# 3) how many different hospitals do the data come from? 

print (len(important_info['InstitutionName'].unique()))
print (important_info['InstitutionName'].unique())


################# one of the patients has more than one series. Use a DICOM viewer suitable to your OS to explore the series 
################# and try explain the differences. 

interesting_patient = important_info[important_info['PatientName'] == str('1.2.840.113619.2.337.3.2831186181.801.1414550448.623')]
two_series = pd.DataFrame(columns = [ds.dir()])
two_series.columns = two_series.columns.get_level_values(0)
two_series['filename'] = ''
cols = [c for c in two_series.columns if not 'pixel' in c.lower()]
cols.remove('AccessionNumber')
two_series = two_series[cols]

files = interesting_patient ['File'].tolist()
i = 0
for f in files:
    filename = get_testdata_files(f)[0]
    dcm_name = filename[-14:]
    ds = pydicom.dcmread(filename)
    filename = dcm_name
    AcquisitionDate = str(ds.AcquisitionDate)
    AcquisitionNumber = str(ds.AcquisitionNumber)
    AcquisitionTime = str(ds.AcquisitionTime)
    AdditionalPatientHistory = str(ds.AdditionalPatientHistory)
    BitsAllocated = str(ds.BitsAllocated)
    BitsStored = str(ds.BitsStored)
    Columns = str(ds.Columns)
    ContentDate = str(ds.ContentDate)
    ContentTime = str(ds.ContentTime)
    ConvolutionKernel = str(ds.ConvolutionKernel)
    DataCollectionDiameter = str(ds.DataCollectionDiameter)
    DerivationCodeSequence = str(ds.DerivationCodeSequence)
    DerivationDescription = str(ds.DerivationDescription)
    DeviceSerialNumber = str(ds.DeviceSerialNumber)
    DistanceSourceToDetector = str(ds.DistanceSourceToDetector)
    DistanceSourceToPatient = str(ds.DistanceSourceToPatient)
    Exposure = str(ds.Exposure)
    ExposureTime = str(ds.ExposureTime)
    FilterType = str(ds.FilterType)
    FocalSpots = str(ds.FocalSpots)
    FrameOfReferenceUID = str(ds.FrameOfReferenceUID)
    GantryDetectorTilt = str(ds.GantryDetectorTilt)
    GeneratorPower = str(ds.GeneratorPower)
    HighBit = str(ds.HighBit)
    ImageOrientationPatient = str(ds.ImageOrientationPatient)
    ImagePositionPatient = str(ds.ImagePositionPatient)
    ImageType = str(ds.ImageType)
    InstanceCreationDate = str(ds.InstanceCreationDate)
    InstanceCreationTime = str(ds.InstanceCreationTime)
    InstanceNumber = str(ds.InstanceNumber)
    InstitutionName = str(ds.InstitutionName)
    IrradiationEventUID = str(ds.IrradiationEventUID)
    KVP = str(ds.KVP)
    Manufacturer = str(ds.Manufacturer)
    ManufacturerModelName = str(ds.ManufacturerModelName)
    Modality = str(ds.Modality)
    NameOfPhysiciansReadingStudy = str(ds.NameOfPhysiciansReadingStudy)
    OperatorsName = str(ds.OperatorsName)
    OtherPatientIDs = str(ds.OtherPatientIDs)
    PatientAge = str(ds.PatientAge)
    PatientBirthDate = str(ds.PatientBirthDate)
    PatientID = str(ds.PatientID)
    PatientName = str(ds.PatientName)
    PatientPosition = str(ds.PatientPosition)
    PatientSex = str(ds.PatientSex)
    PerformedProcedureStepDescription = str(ds.PerformedProcedureStepDescription)
    PerformedProcedureStepID = str(ds.PerformedProcedureStepID)
    PerformedProcedureStepStartDate = str(ds.PerformedProcedureStepStartDate)
    PerformedProcedureStepStartTime = str(ds.PerformedProcedureStepStartTime)
    PerformingPhysicianName = str(ds.PerformingPhysicianName)
    PhotometricInterpretation = str(ds.PhotometricInterpretation)
    PositionReferenceIndicator = str(ds.PositionReferenceIndicator)
    ProtocolName = str(ds.ProtocolName)
    ReconstructionDiameter = str(ds.ReconstructionDiameter)
    ReferencedImageSequence = str(ds.ReferencedImageSequence)
    ReferencedPerformedProcedureStepSequence = str(ds.ReferencedPerformedProcedureStepSequence)
    ReferringPhysicianName = str(ds.ReferringPhysicianName)
    RescaleIntercept = str(ds.RescaleIntercept)
    RescaleSlope = str(ds.RescaleSlope)
    RescaleType = str(ds.RescaleType)
    RevolutionTime = str(ds.RevolutionTime)
    RotationDirection = str(ds.RotationDirection)
    Rows = str(ds.Rows)
    SOPClassUID = str(ds.SOPClassUID)
    SOPInstanceUID = str(ds.SOPInstanceUID)
    ScanOptions = str(ds.ScanOptions)
    SeriesDate = str(ds.SeriesDate)
    SeriesDescription = str(ds.SeriesDescription)
    SeriesInstanceUID = str(ds.SeriesInstanceUID)
    SeriesNumber = str(ds.SeriesNumber)
    SeriesTime = str(ds.SeriesTime)
    SingleCollimationWidth = str(ds.SingleCollimationWidth)
    SliceLocation = str(ds.SliceLocation)
    SliceThickness = str(ds.SliceThickness)
    SoftwareVersions = str(ds.SoftwareVersions)
    SpacingBetweenSlices = str(ds.SpacingBetweenSlices)
    SpecificCharacterSet = str(ds.SpecificCharacterSet)
    StationName = str(ds.StationName)
    StudyDate = str(ds.StudyDate)
    StudyDescription = str(ds.StudyDescription)
    StudyID = str(ds.StudyID)
    StudyInstanceUID = str(ds.StudyInstanceUID)
    StudyTime = str(ds.StudyTime)
    TableHeight = str(ds.TableHeight)
    TotalCollimationWidth = str(ds.TotalCollimationWidth)
    WindowCenter = str(ds.WindowCenter)
    WindowWidth = str(ds.WindowWidth)
    XRayTubeCurrent = str(ds.XRayTubeCurrent)

    two_series.loc[i] = [AcquisitionDate, AcquisitionNumber, AcquisitionTime, AdditionalPatientHistory, 
                         BitsAllocated, BitsStored, Columns, ContentDate, ContentTime, ConvolutionKernel, 
                         DataCollectionDiameter, DerivationCodeSequence, DerivationDescription, DeviceSerialNumber, 
                         DistanceSourceToDetector, DistanceSourceToPatient, Exposure, ExposureTime, FilterType, FocalSpots, 
                         FrameOfReferenceUID, GantryDetectorTilt, GeneratorPower, HighBit, ImageOrientationPatient, 
                         ImagePositionPatient, ImageType, InstanceCreationDate, InstanceCreationTime, InstanceNumber, 
                         InstitutionName, IrradiationEventUID, KVP, Manufacturer, ManufacturerModelName, Modality, 
                         NameOfPhysiciansReadingStudy, OperatorsName, OtherPatientIDs, PatientAge, PatientBirthDate, 
                         PatientID, PatientName, PatientPosition, PatientSex, PerformedProcedureStepDescription, 
                         PerformedProcedureStepID, PerformedProcedureStepStartDate, PerformedProcedureStepStartTime, 
                         PerformingPhysicianName, PhotometricInterpretation, PositionReferenceIndicator, ProtocolName, 
                         ReconstructionDiameter, ReferencedImageSequence, ReferencedPerformedProcedureStepSequence, 
                         ReferringPhysicianName, RescaleIntercept, RescaleSlope, RescaleType, RevolutionTime, RotationDirection, 
                         Rows, SOPClassUID, SOPInstanceUID, ScanOptions, SeriesDate, SeriesDescription, SeriesInstanceUID, 
                         SeriesNumber, SeriesTime, SingleCollimationWidth, SliceLocation, SliceThickness, SoftwareVersions, 
                         SpacingBetweenSlices, SpecificCharacterSet, StationName, StudyDate, StudyDescription, StudyID, 
                         StudyInstanceUID, StudyTime, TableHeight, TotalCollimationWidth, WindowCenter, WindowWidth, 
                         XRayTubeCurrent, filename]
    i +=1

    
wanted_cols = []
for col in two_series.columns:
    if len(two_series[col].unique()) == 2:
           wanted_cols.append(col)

wanted_two_series = two_series[wanted_cols]

wanted_two_series.to_csv(data_path + '\Patients with 2 Series.csv', encoding='utf-8', index=False)



